..  This work is licensed under a Creative Commons Attribution 3.0 Unported
    License.
    http://creativecommons.org/licenses/by/3.0/legalcode

==================================================================
Windows Active Directory Authentication Support for Kubernetes API
==================================================================

Storyboard: https://storyboard.openstack.org/#!/story/2006711

This story implements support for using Windows Active Directory for
authentication of the Kubernetes cluster.

Problem Description
===================

Currently, authentication is done through service accounts. This is typically
only used for Applications to authenticate with Kubernetes, and not end users.
This spec improves the authentication functionality by
adding support for Windows Active Directory for authentication of the
Kubernetes cluster.

Use Cases
=========

StarlingX administrators can optionally deploy a system application
to support using a remote Windows Active Directory for authentication of the
Kubernetes API. The kubernetes cluster can be configured to use this remote
Windows Active Directory authentication at the same time as continuing to
support service accounts for authentication.

Proposed change
===============

Dex will be brought in to StarlingX:
https://github.com/dexidp/dex
Dex is an identity service that uses OpenID Connect to drive authentication
for other apps. Dex acts as a portal to other identity providers through
"connectors". The existing Dex LDAP connector will be used for interfacing
with the remote Windows Active Directory.

We will deploy dex as a containerized StarlingX application. This will be a
system managed application that is automatically uploaded, similar to
platform-integ-apps. We will leverage the dex helm chart from the official
curated helm chart repository. Packaging for this will be in a new repo called
"oidc-auth-armada-app".

The additional OIDC parameters required for dex and its LDAP Connector should
be configurable at bootstrap time. The kube-apiserver parameters
oidc-groups-claim and oidc-ca-file should be added on top of the existing oidc
parameters. The remaining dex configuration should be specified as application
overrides by the user when deploying the oidc-auth application. It should be
possible to update these parameters post-install in a way that survives an
upgrade.

A CLI client for interacting with dex will be included as part of the
deployable application. The CLI client will authenticate a user and get a
token for use with the Kubernetes API. It will then update the kubectl
config file with the token it got. This removes the need for a browser when
authenticating with the Kubernetes cluster. The CLI client should be usable
remotely as well as locally.


Alternatives
============

A number of other open-source approaches are available such as Keycloak and
kube-ldap-authn. Dex was chosen for two main reasons.

* dex provides an OpenID Connect authentication interface. OIDC support
  already exist natively in Kubernetes.

* Dex acts as a portal to other identity providers through "connectors"
  which allows us to more easily support back ends other than Windows Active
  Directory in the future.

Data model impact
=================

None

REST API impact
===============

None

Security impact
===============

This feature adds an additional authentication mechanism to the Kubernetes
cluster.

Other end user impact
=====================

None

Performance Impact
==================

None

Other deployer impact
=====================

Ansible playbook will be updated to include new OIDC fields used by this
feature.

Developer impact
=================

None

Upgrade impact
===============

Kube-apiserver parameters needs to survive an upgrade and still be present
in the new system.

Implementation
==============

Assignee(s)
===========

Primary assignee:

* Jerry Sun (jerrysun)

Other contributors:

* Teresa Ho (teresaho)

Repos Impacted
==============

* oidc-auth-armada-app
* stx-config
* ansible-playbooks

Work Items
===========

* Create new repo for the new application

* Package and build a new platform application for oidc-auth, leveraging
  helm charts for dex from the official curated helm chart repository

* Explore options to implement a CLI based client to interact with dex

* Ansible-playbooks needs to be modified to support the new kube-apiserver
  parameters

* sysinv needs to be modified to support the new oidc-auth application
  helm overrides as well as automatic application upload

Dependencies
============

Kubernetes cluster
User-deployed remote Windows Active Directory to communicate with dex

Testing
=======

The user should be able to deploy their own Windows Active Directory remotely
and set overrides for the oidc-auth application to communicate with it. The
user should then be able to authenticate with their Kubernetes cluster using
credentials specified by their remote Windows Active Directory deployment

Service accounts must also still function properly, for example,

* When Windows Active Directory is not configured, authentication with service
  accounts continues to function correctly.

* When Windows Active Directory is configured, authentication with service
  accounts continues to function correctly.

* When Windows Active Directory is configured and the Windows Active Directory
  is down or inaccessible, authentication with service accounts
  (and the kube-apiserver in general) continues to function correctly.

Documentation Impact
====================

New kube-apiserver parameters mentioned in the documentation. A section
should be added to describe the overrides the user can provide for the
oidc-auth application.

References
==========

None

History
=======

.. list-table:: Revisions
         :header-rows: 1

   * - Release Name
     - Description
   * - stx-4.0
     - Introduced
