..
  This work is licensed under a Creative Commons Attribution 3.0 Unported
  License. http://creativecommons.org/licenses/by/3.0/legalcode

..

=================================================
StarlingX: Ansible Provision for StarlingX Hosts
=================================================

This spec will improve the configurations after system bootstrap sucessfully.


Problem description
===================

The primary controller is currently bootstrapped by ansible, and remote subcloud
bootstrap is supported. However, there are many configurations that need to be
executed manually according to `Installation Guides`_. And in distributed cloud,
the subclouds cannot be configured successfully all at once, after one subcloud
bootstrap, operator have to manually provision subcloud's primary controller.
It is necessary to provide a standard ansible playbook for host provisioning, so
that is the last step to support zero touch deployment completely.

.. _Installation Guides: https://docs.starlingx.io/deploy_install_guides/index.html

Use Cases
---------

* As a developer/tester/operator, I need the ability to configure the hosts all
  by yaml config file.
* As a developer/tester/operator, I need the ability to deploy the subcloud with
  one touch.
* As a developer of StarlingX community, I would like the deployment more easier
  just with inputting required parameters.

Proposed change
===============

Existing workflow
-----------------
**Playbook:**

1. Apply bootstrap puppet manifest
2. Populate system configuration (with defaults and user-supplied config)
3. Bring up Kubernetes master node and essential services

**Host-configuration:**

   Manual or scripted configurations required for unlock.

**Host-unlock**

1. Apply controller puppet manifest (and worker, storage puppet manifests
   for All-in-one)
2. Activate all services

Proposed workflow
-----------------
The provision of the StarlingX hosts will be orchestrated by an Ansible
Playbook.

**Playbook:**

1. Apply bootstrap puppet manifest
2. Populate system configuration (with defaults and user-supplied config)
3. Bring up Kubernetes master node and essential services

**Host-configuration:**

1. Configure network interfaces
2. Configure ceph cluster
3. Configurations of Kubernetes and OpenStack

**Host-unlock**

1. Apply controller puppet manifest (and worker, storage puppet manifests
   for All-in-one)
2. Activate all services

Scope of the new workflow
-------------------------
The new workflow will cover the **customize configurations** after bootstrap.

Provision playbook roles and tasks
----------------------------------

Below is a list of major roles and tasks.

Role: validate-config-input
   * Task: validate-config
Role: configure-interfaces
   * Task: configure-oam-interface
   * Task: configure-mgmt-interface
   * Task: configure-cluster-interface
   * Task: configure-data-interfaces
Role: configure-ceph
   * Task: configure-ceph-mon
   * Task: configure-ceph-osd
Role: configure-kubernetes
   * Task: configure-k8s-sriov
Role: configure-openstack
   * Task: configure-nova-local
   * Task: configure-vswitch
   * Task: configure-host-labels
Role: unlock-hosts
   * Task: check-configurations
   * Task: unlock-host

Playbook directory layout
-------------------------
The directory layout of the playbook initially could be as follows:

provision.yml

roles/
  provision/
    validate-config-input/
      tasks/
        main.yml

    configure-interfaces/

    configure-ceph/

    configure-kubernetes/

    configure-openstack/

    unlock-hosts/

Playbook pre_tasks and post_tasks
---------------------------------
The pre_tasks and post_tasks can be as simple as marking the start and end
of the playbook execution.

Running ``provision playbook``
------------------------------
ansible-playbook provision.yml -i <inventory-file> -e <variable-file>
-l <limit-hosts>

Alternatives
------------

None

Data model impact
-----------------

None

REST API impact
---------------

The dcmanager API will be modified to support the following operations:

* **POST /v1.0/subclouds**

  * Now, a new request to create subcloud will have two other optional
    parameters, "deploy_playbook" and "deploy_values". "deploy_playbook"
    is used for inputting an executable ansible playbook and "deploy_values"
    is the parameters for this playbook. When they are both defined, the
    'deploy_playbook' will be executed after subcloud controller-0
    bootstrap. At presnet, the "deploy_playbook" and "deploy_values" must
    be created by every operator. So I hope to develop a common ansible
    playbook, it can be used to provision not only central's nodes but also
    subclouds' nodes. When you want to use the common provisioning playbook,
    just define "deploy_values". In addition, you also can define both
    "deploy_playbook" and "deploy_values" to use customize provisioning
    playbook, and not define both to not provision.
  * For implementing the functionality above, we need to update the
    back-end codes for allowing that "deploy_values" is defined with
    undefined "deploy_playbook" which case is not allowed now.

Security impact
---------------

Provisioning is executed after bootstrap by ansible. As such, it does not
introduce any additional security impact.

Other end user impact
---------------------

The user will be expected to interact with the feature using
ansible-playbook to proform host unlock. The provision deployment
method will give the user easier to customize and automate the deployment.

Performance Impact
------------------

Using ansible to unlock host should be faster and more reliable than manual.

Other deployer impact
---------------------

See end user impact.

Developer impact
----------------

None

Upgrade impact
--------------

None

Implementation
==============

Assignee(s)
-----------

Primary assignee:
  Zhang Kunpeng(zhang.kunpeng@99cloud.net)

Other contributors:
  Hu Tianhao(hu.tianhao@99cloud.net)

Repos Impacted
--------------

* ansilbe-playbook
* discloud

Work Items
----------

* Create a ``provision`` Playbook to unlock primary host and other hosts.
* Modify discloud to support provision playbook when creating subcloud.


Dependencies
============

* Containerized StarlingX based deployment


Testing
=======

This story does not affect the configuration and deployment of StarlingX, it
makes end user easier to configure hosts.

The provision tests will be limited to All-in-one simplex,All-in-one duplex,
Standard configurations and Distributed Cloud configurations.

Documentation Impact
====================

This story affects the StarlingX installation and configuration
documentation. Specific details of the documentation changes will be
addressed once the implementation is complete.

References
==========

None


History
=======

.. list-table:: Revisions
   :header-rows: 1

   * - Release Name
     - Description
   * - Stein
     - Introduced
