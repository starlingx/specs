..
  Copyright (c) 2022 Marvell International Ltd.

..

==========================================
Marvell Octeon RAN Accelerator Integration
==========================================

Storyboard:
https://storyboard.openstack.org/#!/story/2010047

Marvell supports Octeon line of SoCs to be used as a basic NIC or
RAN Accelerator, when configured in Endpoint mode. Marvell developed PCI SR-IOV
NIC drivers (PF and VF) for these products. The NIC drivers on host create a
netdev interface for PF device and supports creating VFs.  Octeon SoCs feature
multiple ARM v9 cores along with Ethernet, PCIe and baseband PHY (RAN).
This enables users to offload L1 processing to Octeon SoC.

This specification describes what needs to be done to intergrate
Marvell's PCIe NIC drivers (PF and VF) into StarlingX.

Problem description
===================

Currently, StarlingX does not include support for Marvell RAN Accelerator card.
This specification aims at integrating the Marvell PCIe NIC drivers into
StarlingX platform to enable support for Marvell RAN accelerator cards.

Use Cases
---------

To enable users to deploy Octeon as a basic NIC or RAN Accelerator on a
StarlingX cloud installation.


Proposed change
===============

This change adds following new drivers and tools:
  #. SRIOV PF and VF drivers. These drivers enable netdev communication between l2/l3 running on the host cpu and l1 running on the accelerator
  #. CNI plugin support for Marvell RAN Accelerator cards
  #. Integrate NIC firmware update tool


Alternatives
------------

For PF and VF drivers, there are no alternatives for this as the device drivers
are the only way of initializing and operating these devices.

For CNI plugin, we will not be adding CNI plugin support from scratch.
We will be leveraging the existing CNI plugin to enable suppoort for
Marvell devices too.

For NIC firmware update tool, the tool will be able to update firmware on
Marvell RAN Accelerator from host through PCIe interface, without needing
any drivers to be loaded. There is no alternate method for this.

Data model impact
-----------------

No data model impact expected.

REST API impact
---------------

This change will not add or change any REST APIs.

Security impact
---------------

None

Other end user impact
---------------------

User will be invoking the firmware update tool from host to update boot image,
kernel or root filesystem on the Marvell RAN Accelerator card.

Performance Impact
------------------

None; These drivers will only schedule work queue task to handle periodic
heartbeat messages from Octeon card, and this is not CPU intensive.

Other deployer impact
---------------------

None

Developer impact
----------------

None

Upgrade impact
--------------

None

Implementation
==============

Assignee(s)
-----------

Primary assignee:
  Veerasenareddy Burru

Other contributors:
  Abhijit Ayrekar
  Sathesh Edara
  Satananda Burla

Repos Impacted
--------------

starlingx/config
starlingx/fault
starlingx/integ
starlingx/kernel

Work Items
----------

* Integrate the kernel SRIOV PF driver
* Integrate the kernel SRIOV VF driver
* Integrate NIC firmware update tool
* Add CNI plugin support for Marvell RAN Accelerator NIC

Dependencies
============

None

Testing
=======

* Testing will be performed on Marvell RAN Accelerator hardware.
* Testing will include basic PF NIC functionality, SRIOV VF creation,
  VF NIC functionality.
* Unit tests will be provided on how to test the L1/L2 communication.

Documentation Impact
====================

* End User guide need to be updated on how to run the marvell NIC firmware
  update tool.

References
==========

The below product brief describes the family of Octeon CNF95XX in 5G RAN environment.
https://www.marvell.com/content/dam/marvell/en/public-collateral/embedded-processors/marvell-infrastructure-processors-octeon-fusion-cnf95xx-product-brief.pdf

History
=======

.. list-table:: Revisions
   :header-rows: 1

   * - Release Name
     - Description
   * - 1
     - Introduced
