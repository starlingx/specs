..
  This work is licensed under a Creative Commons Attribution 3.0 Unported
  License. http://creativecommons.org/licenses/by/3.0/legalcode

=============================
Separate CA for k8s and etcd
=============================
Storyboard:
https://storyboard.openstack.org/#!/story/2008833

This proposal will separate CA for k8s and etcd

Problem description
===================
Currently, we are reusing kubernetes-ca for etcd but not using a separate CA.
Making one single root CA by copying the root certificate/key would blur the
boundary of certificates for apiserver and etcd.
It may cause unforeseen complexities for future features. kubernetes-ca and
etcd-ca have different purposes and usage.
Sharing cert between kubernetes-ca and etcd-ca will dramatically increase the
complexity of updating etcd-ca cert.
We'd better refer to the best practice for k8s and etcd certs in [1]_

Use Cases
---------

* Admin wants to use a separate external root CA for etcd and k8s.
* Admin wants to maintain the root CA for etcd and k8s separately

Proposed change
===============

**Fresh Deployment**

#.  Generate self-signed CA certificate for etcd before apply bootstrap manifest

    Generate a unique CA for etcd and related certs.
        Copy apiserver-etcd-client.crt/key to /etc/kubernetes/pki/
        Copy etcd ca and etcd-server crt/key to /etc/etcd/
        Do not modify the Kubernetes-ca in /etc/kubernetes/pki/
        Change etcd-ca and certs paths, that will be used by api-server in
        playbookconfig/src/playbooks/roles/common/files/kubeadm.yaml.erb

**Changes for upgrade process**


* Simplex

  For simplex upgrade, it will use restore procedure during installing the new starlingx
  software. So we need to generate etcd-ca separately before apply bootstrap manifest for
  etcd during restore procedure, that is similar like we do for fresh deployment.

* Duplex/Multi-node
  During upgrade-activate stage, we will do below
  1) Create /etc/kubernetes/pki/etcd/ subfolder, and generate etcd-ca and related certs
  2) Copy etcd-ca and related certs to /etc/kubernetes/pki/etcd/ and /etc/etcd/
  3) Restart etcd, and update the config of api-server before restart it.

Alternatives
------------

None

Data model impact
-----------------

None

REST API impact
---------------

None

Security impact
---------------

It can reduce the security risk after separate k8s CA and etcd CA

Other end user impact
---------------------

none

Performance Impact
------------------

none

Other deployer impact
---------------------

Admin user is able to use separate external root ca cert for k8s and etcd

Developer impact
----------------

None

Upgrade impact
--------------

It has already been described in section `Proposed change`_

Implementation
==============

Assignee(s)
-----------

Primary assignee:

Repos Impacted
--------------

ansible-playbooks
stx-puppet
config

Work Items
----------

The work items have already been described in section `Proposed change`_

Dependencies
============

None


Testing
=======
* Deployment test on both simplex and duplex.
* Switch active controller.
* Lock/unlock of a simplex controller.
* Backup/Restore test on simplex and duplex.
* Spontaneous reboot of a simplex controller.
* Re-installing a controller host on a duplex setup and
  then swacting to it.
* Upgrade test on both simplex and duplex.


Documentation Impact
====================



References
==========

.. [1]  https://kubernetes.io/docs/setup/best-practices/certificates/


History
=======

.. list-table:: Revisions
   :header-rows: 1

   * - Release Name
     - Description
   * - stx.6.0
     - Separate CA for k8s and etcd
