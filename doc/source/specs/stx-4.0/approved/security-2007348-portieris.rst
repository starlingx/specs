..
  This work is licensed under a Creative Commons Attribution 3.0 Unported
  License. http://creativecommons.org/licenses/by/3.0/legalcode

..

==========================================
Portieris integration
==========================================

Storyboard: https://storyboard.openstack.org/#!/story/2007348

This story integrates Portieris ( https://github.com/IBM/portieris ), a
Kubernetes admission controller for the enforcement of image security policies
into the platform, to be deployed as an armada app.

Problem description
===================

When launching a pod, it may pull docker images from a variety of container
registries.  Because containers share the host kernel and depending on pod
security contexts may have access to some host resources, it is an important
security concept to 'trust' the container images running in containers on your
system. In order to confirm the integrity and the publisher of container
images, it is necessary to allow the enforcement of image security policies.

Use Cases
---------

* Deployer wants to restrict to only authorized images signed by a particular
  publisher being deployed.
* End user wants to validate the integrity of the container images being used.

Proposed change
===============

Portieris is an open-source project that serves as a Kubernetes Admission
Webhook, to validate Docker Content Trust through communicating with a notary
server and prevent resource creation with unauthorized or integrity-compromised
container images.

This will introduce a new armada app called 'portieris-armada-app', which can
be installed through sysinv, and will deploy portieris through its helm charts.

Once installed, image security policies will be able to be configured through
kubectl.

This will also require creating a 'portieris-armada-app' repo, which will
contain the source and build information for this armada app.

It will also be necessary to fix any blocking bugs in portieris that are
affecting the correct deployment/behavior of this armada app including with
open-source docker registries such as harbor.

Alternatives
------------

open policy agent gatekeeper, plus implementing new service to talk to notary
server.  Would be much more complex than using portieris, which is made for
this use case.

grafeas/kritis, which would require adding grafeas to the project, a much more
significant change than adding portieris as an armada app.

Data model impact
-----------------

None

REST API impact
---------------

None

Security impact
---------------

Introduces new security feature, while will authenticate container images being
used.

Uses notary libraries to communicate with a notary server, which use
cryptography (both encryption and hashing) for this connection.  Also does
cryptographic signature verification.

User-provided notary configuration provided as part of bootstrap.

Other end user impact
---------------------

Deployed through armada app.  If applied, restricts end users to only
deployments that comply with configured image security policies.

Performance Impact
------------------

If the armada app is applied, this may have a small impact on pod creation
times, due to the extra request over the network to the notary server.
This armada app will be optional, so should not be used in deployments where
this impact is prohibitive.

Other deployer impact
---------------------

When configured, it is required to have an external notary server.  If
connectivity to this notary server is lost, pod creation will not be allowed.

Developer impact
----------------

None

Upgrade impact
--------------

None

Implementation
==============

Assignee(s)
-----------

Primary assignee:
  ~josephrichard


Repos Impacted
--------------
* portieris-armada-app repo needs to be created.
* stx-config will have helm plugins added, which should eventually be moved to
  armada app repo.

Work Items
----------
* Introduce new 'portieris-armada-app' repo and app, following current pattern
  used by StarlingX for other armada apps, with helm charts pulled from latest
  portieris release on github.

* Integrate with cert-manager, with secrets managed through cert-manager.

* Fix/upstream any issues with portieris related to open source docker
  registries such as harbor.

* Fix/upstream proper overriding of ca cert in portieris.

Dependencies
============

* portieris - https://github.com/IBM/portieris
* cert-manager - https://storyboard.openstack.org/#!/story/2007360

Testing
=======

No unit tests will be added to StarlingX.  Upstream portieris project contains
unit tests in github repo, and additional unit tests will be added alongside
upstream bug fixes.

Pre-submission tests will be run using various docker registries:

* harbor
* jfrog

Documentation Impact
====================

Add section on configuring portieris image policies.  Details on specification
of image policies should reference portieris documentation.

References
==========

* storyboard: https://storyboard.openstack.org/#!/story/2007348
* portieris: https://github.com/IBM/portieris
* image policy configuration:
  https://cloud.ibm.com/docs/services/Registry?topic=registry-security_enforce

History
=======

.. list-table:: Revisions
   :header-rows: 1

   * - Release Name
     - Description
   * - stx-4.0
     - Introduced
