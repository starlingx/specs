..
  This work is licensed under a Creative Commons Attribution 3.0 Unported
  License. http://creativecommons.org/licenses/by/3.0/legalcode

============================================
O-RAN Specification Compliant O2 Interfaces
============================================

Storyboard:
https://storyboard.openstack.org/#!/story/2010278


To facilitate the integration of components in context of O-RAN architecture,
O-RAN Alliance publishes a set of specifications to normalize the interfaces
between them, as far as O-Cloud concerned, the O2 interfaces are specified
between O-Cloud and O-RAN SMO. As part of effort to make StarlingX be compliant
to O-RAN specifications, oran O2 service is developed and integrated into
StarlingX (either standalone mode or distributed cloud mode) to expose O2
interfaces towards to SMO.

Problem description
===================

Users want to integrate StarlingX as O-Cloud to O-RAN SMO via O2 interfaces
published by O-RAN Specifications as below:

`WG6: O-RAN O2ims Interface Specification v3.00 <https://oranalliance.atlassian
.net/wiki/spaces/TSC/pages/2570617688/APPROVED+-+WG6+O-RAN+O2ims+Interface+
Specification+v3.00>`_

`WG6: O-RAN O2dms Interface Specification: Kubernetes Native API Profile for
Containerized NFs v2.00 <https://oranalliance.atlassian.net/wiki/spaces/TSC/
pages/2570715602/APRROVED+-+WG6+O-RAN+O2dms+Interface+Specification+Kubernetes+
Native+API+Profile+for+Containerized+NFs+v2.00>`_

Hence it is expected that following O2 services are deployed over StarlingX:

* O2ims_InfrastructureInventory
* O2ims_InfrastructureMonitoring
* O2dms in Kubernetes native API profile

Use Cases
---------

This feature addresses several use cases specified by:
`WG6: O-RAN Cloudification and Orchestration Use Cases and Requirements for
O-RAN Virtualized RAN v5.00 <https://oranalliance.atlassian.net/wiki/spaces/
TSC/pages/2569831376/APPROVED+-+WG6+O-RAN+Cloudification+and+Orchestration+
Use+Cases+and+Requirements+for+O-RAN+Virtualized+RAN+v5.00>`_

Specifically, the following Use cases and Requirements should be realized:

* O-Cloud Registration and Initialization Use Case
* O-Cloud Inventory Update Use Case
* Network Function Basic Use Cases
* Fault Use Cases

The use cases above should be applicable to StarlingX in either standalone
mode or distributed cloud mode.

Proposed change
===============

An `oran-o2 service <https://docs.o-ran-sc.org/projects/o-ran-sc-pti-o2/
en/latest/index.html>` has been developed as part of INF project of o-ran
software community.

This oran-o2 service has been designed and implemented to host the o2ims
services including O2ims_InfrastructureInventory,
O2ims_InfrastructureMonitoring, and other O2 services in future to represent
StarlingX as an O-RAN Specifications compliant O-Cloud.

This oran-o2 service will be packaged as a new FluxCD based containerized 
application. Users decide whether to deploy this oran-o2 service and when.
In case of StarlingX in distributed cloud mode, oran-o2 will be deployed
over system controller only.

Fundamentally, the oran-o2 services act as a mapping layer to map between
StarlingX resources and corresponding service objects of O-RAN specifications.

Specifically, the following O-RAN service objects are mapped to StarlingX
resources:

=================  ================================ ===========================
O-RAN model        StarlingX in standalone mode     StarlingX in distributed
                                                    cloud mode
=================  ================================ ===========================
O-Cloud            a StarlingX instance             a System Controller and all
                                                    subclouds it manages
ResourceType       StarlingX sysinv API resource    StarlingX sysinv API
                   kind                             resource kind, refer /o2ims
                                                    -infrastructureInventory/v1
                                                    /resourceTypes for examples
ResourcePool       a StarlingX instance             a instance of subcloud,
                                                    refer to /o2ims-
                                                    infrastructureInventory/v1
                                                    /resourcePools for examples
Resource           Specific instance of StarlingX   Specific instance of
                   sysinv API resource              sysinv API resource,
                                                    refer to /o2ims-
                                                    infrastructureInventory/v1
                                                    /resourcePools/
                                                    {resourcePoolID}/resources
                                                    for examples
DeploymentManager  a single Kubernetes cluster      a single Kubernetes cluster
                   hosted by a StarlingX instance   hosted by a subcloud,
                                                    refer to /o2ims-
                                                    infrastructureInventory/v1
                                                    /deploymentManagers
                                                    for examples
AlarmDefinition    Fault Management Alarm Message   Fault Management Alarm
                   definition                       Message definition
AlarmEventRecord   an occurrence of Fault           an occurrence of Fault
                   Management Alarm Message         Management Alarm Message
AlarmSeverity      Fault Management Alarm Severity  Fault Management Alarm
                                                    Severity
=================  ================================ ===========================

..
  The resources of RegionOne of system controller can also be
  represented as a ResourcePool as well as DeploymentManager

As implementation of o2ims services, the oran o2 service monitors sysinv
resources via sysinv api, builds a cache of resources compliant to O-RAN O2
model as inventory and persists them into internal database.

On the other hand, oran o2 service exposes this inventory to SMO through a set
of O2 APIs for querying resources as well as subscribing to get notification upon
the inventory changes and alarm changes.

As implementation of o2dms, the kubernetes clsuters hosted by StarlingX will
be exposed as O2dms in kubernetes native API profile, while the access
information of these clusters are exposed as deploymentmanager object as part
of o2ims inventory. So SMO could query the it to extract the access
crendentials, and interact directly with the specific kubernetes cluster with
kubernetes native APIs.

To make oran o2 service survive through the migration of oran-o2 pods among
different StarlingX hosts, the inventory database is persisted by leveraging
kubernetes PV volumes.

Alternatives
------------

It has been considered to couple the o2ims service with sysinv service to
optimize the efficiency of monitoring sysinv resource changes. However, given
that O-RAN specifications is still in early stage of its rapid evolution
progress, it is decided to decouple these two services and integrating them
by utilizing existing sysinv API for now.

Data model impact
-----------------

As part of the o2ims inventory, a new group of tables will be introduced to
persist O-RAN modeling compliant objects representing StarlingX sysinv resources.
This inventory is internally managed as part of deployment of oran o2 service.
The diagram below shows the new table names and relationships.

..
                       +-----------+
                 +-----+ ocloud    +---+
                 |     +-----------+   |
                 |                     |
                 |                     |
                 |N                    |N
    +------------+------+   +----------+-----+ +------------------+
    |                   |   | resourcepool   | | alarmSubscription|
    |deploymentmanager  |   |                | |                  |
    +-------------------+   +-------+--------+ +------------------+
                                    |
      +--------------+              |
      | subscription |              |N
      +--------------+      +-------+--------+    +-----------------+
                            | resource       |1   |alarmEventRecord |
     +---------------+      |                +----+                 |
     | configuration |      +------+---------+    +------+----------+
     +---------------+             |                     |
                                   |                     |
                                   |1                    |
                            +------+---------+ 1         |
                            | resourcetype   +-----------+
                            +----------------+

The resourcetype table is populated with predefined resourcetypes as below:
- pserver
- pserver_cpu
- pserver_mem
- pserver_ethernet
- pserver_if

The resource table contains specific o2ims resources which represents the
corresponding sysinv resource accordingly to predefined resourcetypes. The
resource are organized in a hierachical way to represent the relationship
between each other, e.g. an instance of pserver_cpu refers to a instance of
pserver with its parentid. an instance of pserver refers to an instance of
resourcepool which represents the subcloud.

The resourcepool table contains representations of each subcloud.

The deploymentmanager table contains representations of kubernetes cluster
for each subcloud

The ocloud table contains a single representation of StarlingX instance in
either standalone mode or distributed cloud mode.

The subscription table contains information of subscribers which are interested
in receiving notification upon the changes of o2ims inventory.

The configuration table contains provisioning information, e.g., smo endpoint
for o-cloud registration

The alarmSubscription table contains information of subscribers which are
integrated in receiving alarm notification.

The alarmEventRecord table contains information of alarms generated by
StarlingX system.


REST API impact
---------------

Following the spec of O-RAN Alliance as of this writing,
`WG6: O-RAN O2ims Interface Specification v3.00 <https://oranalliance.atlassian
.net/wiki/spaces/TSC/pages/2570617688/APPROVED+-+WG6+O-RAN+O2ims+Interface+
Specification+v3.00>`_, the following APIs will be added:

o2ims-infrastructureInventory/v1
________________________________

GET o2ims-infrastructureInventory/v1

RESP:

..
  {
    "oCloudId": "42700a14-21d6-405c-a362-e691b20656b1",
    "globalcloudId": cfac6bda-21d6-405c-a362-e691b20656b1,
    "name": "StarlingX instance name",
    "description": "An ocloud",
    "serviceUri":
    "http://128.224.115.35:30205/o2ims-infrastructureInventory/v1/"
  }

/o2ims-infrastructureInventory/v1/resourceTypes
_______________________________________________

GET /o2ims-infrastructureInventory/v1/resourceTypes

RESP:

..
  [
    {
      "resourceTypeId": "dcce9bcc-fb38-4e49-bf84-4f1519f3b031",
      "name": "pserver",
      "vendor": null,
      "version": "",
      "description": "",
      "alarmDictionary": {}
    },
    {
      "resourceTypeId": "f5bdc0a0-eee7-426a-8d07-b56f950a94d2",
      "name": "pserver_cpu",
      "vendor": null,
      "version": "",
      "description": "",
      "alarmDictionary": {}
    },
    {
      "resourceTypeId": "0d24f2ce-a0fa-460f-a65c-541b694110e5",
      "name": "pserver_mem",
      "vendor": null,
      "version": "",
      "description": "",
      "alarmDictionary": {}
    },
    {
      "resourceTypeId": "7a150a2d-5107-4bfc-ada8-c6c4b037430c",
      "name": "pserver_ethernet",
      "vendor": null,
      "version": "",
      "description": "",
      "alarmDictionary": {}
    },
    {
      "resourceTypeId": "63557eda-f8e1-4a52-82fa-f3e6f52fea9f",
      "name": "pserver_if",
      "vendor": null,
      "version": "",
      "description": "",
      "alarmDictionary": {}
    }
  ]

/o2ims-infrastructureInventory/v1/resourceTypes/{resourceTypeID }
_________________________________________________________________

GET /o2ims-infrastructureInventory/v1/resourceTypes/
dcce9bcc-fb38-4e49-bf84-4f1519f3b031

RESP:

..
  {
    "resourceTypeId": "dcce9bcc-fb38-4e49-bf84-4f1519f3b031",
    "name": "pserver",
    "vendor": "vendor1",
    "version": "v1",
    "description": "desc1",
    "alarmDictionary": {}
  }

/o2ims-infrastructureInventory/v1/resourcePools
_______________________________________________

GET /o2ims-infrastructureInventory/v1/resourcePools

RESP:

..
  [
    {
      "resourcePoolId": "42700a14-21d6-405c-a362-e691b20656b1",
      "name": "subcloud1",
      "globalLocationId": "42700a14-21d6-405c-a362-e691b20656b1",
      "location": "location1",
      "description": "A Resource Pool",
      "oCloudId": ""
    }
  ]


/o2ims-infrastructureInventory/v1/resourcePools/{resourcePoolID}
________________________________________________________________

GET /o2ims-infrastructureInventory/v1/resourcePools/
42700a14-21d6-405c-a362-e691b20656b1

RESP:

..
  {
    "resourcePoolId": "42700a14-21d6-405c-a362-e691b20656b1",
    "name": "subcloud1",
    "globalLocationId": "42700a14-21d6-405c-a362-e691b20656b1",
    "location": "location1",
    "description": "A Resource Pool",
    "oCloudId": ""
  }


/o2ims-infrastructureInventory/v1/resourcePools/{resourcePoolID}/resources
__________________________________________________________________________
?{filters}
__________

GET o2ims-infrastructureInventory/v1/resourcePools/
42700a14-21d6-405c-a362-e691b20656b1/resources?resourceTypeName=pserver

RESP:

..
  [
    {
      "resourceId": "67e71e16-6264-4ac0-b57f-815cf6b7dc70",
      "resourceTypeId": "dcce9bcc-fb38-4e49-bf84-4f1519f3b031",
      "resourcePoolId": "42700a14-21d6-405c-a362-e691b20656b1",
      "name": "controller-0",
      "parentId": null,
      "description": "A physical server resource",
      "globalAssetId": ""
    },
    {
      "resourceId": "54d74187-775e-46dd-b8b0-5875a14223d8",
      "resourceTypeId": "dcce9bcc-fb38-4e49-bf84-4f1519f3b031",
      "resourcePoolId": "42700a14-21d6-405c-a362-e691b20656b1",
      "name": "controller-1",
      "parentId": null,
      "description": "A physical server resource",
      "globalAssetId": ""
    }
  ]

o2ims-infrastructureInventory/v1/resourcePools/{resourcePoolID}/resources/
__________________________________________________________________________
{resourceID}
____________

GET o2ims-infrastructureInventory/v1/resourcePools/
42700a14-21d6-405c-a362-e691b20656b1/resources/
67e71e16-6264-4ac0-b57f-815cf6b7dc70

RESP:

..
  {
    "resourceId": "67e71e16-6264-4ac0-b57f-815cf6b7dc70",
    "resourceTypeId": "dcce9bcc-fb38-4e49-bf84-4f1519f3b031",
    "resourcePoolId": "42700a14-21d6-405c-a362-e691b20656b1",
    "name": "controller-0",
    "parentId": null,
    "description": "A physical server resource",
    "globalAssetId": "",
    "elements": "",
    "children": [
      {
        "resourceId": "d380122a-fa29-4813-a77a-8b36a1aa257d",
        "resourceTypeId": "f5bdc0a0-eee7-426a-8d07-b56f950a94d2",
        "resourcePoolId": "42700a14-21d6-405c-a362-e691b20656b1",
        "name": "67e71e16-cpu-0",
        "parentId": "67e71e16-6264-4ac0-b57f-815cf6b7dc70",
        "description": "A CPU resource of the physical server",
        "elements": "",
        "children": null
      }
    ]
  }


/o2ims-infrastructureInventory/v1/subscriptions
_______________________________________________

POST /o2ims-infrastructureInventory/v1/subscriptions

..
  {
    "callback": "https://1.2.3.4/smo_observer",
    "consumerSubscriptionId": "observerid1",
    "filter": "filter1=value1"
  }

RESP:

..
  {
    "subscriptionId": "7900907e-b60a-4cfd-b6a5-dae760a865f0"
  }

GET /o2ims-infrastructureInventory/v1/subscriptions

RESP:

..
  [
    {
      "subscriptionId": "7900907e-b60a-4cfd-b6a5-dae760a865f0",
      "callback": "https://1.2.3.4/smo_observer",
      "consumerSubscriptionId": "observerid1",
      "filter": "filter1=value1"
    }
  ]

/o2ims-infrastructureInventory/v1/subscriptions/{subscriptionID}
________________________________________________________________

GET /o2ims-infrastructureInventory/v1/subscriptions/
7900907e-b60a-4cfd-b6a5-dae760a865f0

RESP:

..
  {
    "subscriptionId": "7900907e-b60a-4cfd-b6a5-dae760a865f0",
    "callback": "https://1.2.3.4/smo_observer",
    "consumerSubscriptionId": "observerid1",
    "filter": "filter1=value1"
  }

DELETE /o2ims-infrastructureInventory/v1/subscriptions/
7900907e-b60a-4cfd-b6a5-dae760a865f0

RESP: None

/o2ims-infrastructureInventory/v1/deploymentManagers
____________________________________________________

GET /o2ims-infrastructureInventory/v1/deploymentManagers

RESP:

..
  [
    {
      "deploymentManagerId": "9fc43d52-b73a-366a-b08f-e89cbbf82532",
      "name": "cfac6bda-486c-401b-8451-9cb226eb3884.kubernetes",
      "description": "A DMS",
      "serviceUri": "http://128.224.115.35:30205/o2dms/v1/9fc43d52-b73a-366a-b08f-e89cbbf82532",
      "supportedLocations": "",
      "capabilities": "",
      "capacity": "",
      "oCloudId": ""
    }
  ]

/o2ims-infrastructureInventory/v1/deploymentManagers/{deploymentManagerID}
__________________________________________________________________________

GET /o2ims-infrastructureInventory/v1/deploymentManagers/
9fc43d52-b73a-366a-b08f-e89cbbf82532

RESP:

..
  {
    "deploymentManagerId": "9fc43d52-b73a-366a-b08f-e89cbbf82532",
    "name": "cfac6bda-486c-401b-8451-9cb226eb3884.kubernetes",
    "description": "A DMS",
    "serviceUri": "https://128.224.115.34:6443",
    "supportedLocations": "",
    "capabilities": "",
    "capacity": "",
    "oCloudId": ""
  }

/o2ims-infrastructureMonitoring/v1/alarms
_________________________________________

GET /o2ims-InfrastructureMonitoring/v1/alarms

RESP:

..
  [
    {
      "alarmEventRecordId": "9fc43d52-b73a-366a-b08f-e89cbbf82532",
      "resourceTypeID": "cfac6bda-486c-401b-8451-9cb226eb3884",
      "resourceID": "feac6bda-486c-401b-8451-9cb226eb3123",
      "alarmDefinitionID": "100.101",
      "probableCauseID": "",
      "alarmRaisedTime": "2022-09-01T08:01:12",
      "alarmAcknowledgeTime": "",
      "alarmAcknowledged": false,
      "perceivedSeverity": 3,
      "extensions": []
    }
  ]

/o2ims-infrastructureMonitoring/v1/alarms/{alarmEventRecordID}
______________________________________________________________

GET /o2ims-InfrastructureMonitoring/v1/alarms/
9fc43d52-b73a-366a-b08f-e89cbbf82532

RESP:

..
  {
    "alarmEventRecordId": "9fc43d52-b73a-366a-b08f-e89cbbf82532",
    "resourceTypeID": "cfac6bda-486c-401b-8451-9cb226eb3884",
    "resourceID": "feac6bda-486c-401b-8451-9cb226eb3123",
    "alarmDefinitionID": "100.101",
    "probableCauseID": "",
    "alarmRaisedTime": "2022-09-01T08:01:12",
    "alarmAcknowledgeTime": "",
    "alarmAcknowledged": false,
    "perceivedSeverity": 3,
    "extensions": []
  }

/o2ims-infrastructureMonitoring/v1/alarmSubscriptions
_____________________________________________________

POST /o2ims-infrastructureMonitoring/v1/alarmSubscriptions

..
  {
    "consumerSubscriptionId": "bfec6bda-486c-401b-8451-9cb226eb3999",
    "filter": "severity<=2",
    "callback": "https://1.2.3.4/alarm_observer1"
  }

RESP:

..
  {
    "alarmSubscriptionId": "aef43d52-b73a-366a-b08f-e89cbbf82111",
    "consumerSubscriptionId": "bfec6bda-486c-401b-8451-9cb226eb3999",
    "filter": "severity<=2",
    "callback": "https://1.2.3.4/alarm_observer1"
  }

GET /o2ims-infrastructureMonitoring/v1/alarmSubscriptions

RESP:

..
  [
    {
      "alarmSubscriptionId": "aef43d52-b73a-366a-b08f-e89cbbf82111",
      "consumerSubscriptionId": "bfec6bda-486c-401b-8451-9cb226eb3999",
      "filter": "severity<=2",
      "callback": "https://1.2.3.4/alarm_observer1"
    }
  ]

/o2ims-infrastructureMonitoring/v1/alarmSubscriptions/{alarmSubscriptionId}
___________________________________________________________________________

GET /o2ims-infrastructureMonitoring/v1/alarmSubscriptions/
aef43d52-b73a-366a-b08f-e89cbbf82111

RESP:

..
  {
    "alarmSubscriptionId": "aef43d52-b73a-366a-b08f-e89cbbf82111",
    "consumerSubscriptionId": "bfec6bda-486c-401b-8451-9cb226eb3999",
    "filter": "severity<=2",
    "callback": "https://1.2.3.4/alarm_observer1"
  }

DELETE /o2ims-infrastructureMonitoring/v1/alarmSubscriptions/
aef43d52-b73a-366a-b08f-e89cbbf82111

RESP: None

Security impact
---------------

oran-o2 services exposes API endpoints towards a single SMO which should be
specified during provisioning time, so it is critical to reach mutually trusts
with following approach:

* The SMO API endpoint which is provisioned to oran-o2 for O-Cloud registration
  should be protected by https.
* The oran-o2 API endpoint will be protected by https, the certificate
  management will be manually done for now, and will be integrated with
  cert-manager in future.
* An initial token will be provided to SMO during O-Cloud registration.
* SMO should use that token to request oran-o2 service via API endpoint.
* The token should be renewed before it is expired and dispatch to SMO by
  oran-o2 service
* The token received from SMO via its API requests will be validated against
  authentication provider. The authentication provider is the kubernetes
  APIserver which hosts the oran o2 service, and can be extended in future to
  support other authentication providers, e.g., the openstack keystone service
  of StarlingX.
* The token management will be manually done for now and will be automated
  in future.
* There is no user sensitive data involved.
* There is no need for cryptography, but need hashing for checksum of
  resource information.
* There is no need for sudor or any elevated privileges.
* The oran-o2 requires openstack admin permission to interact with sysinv APIs.
* The oran-o2 requires a kubernetes service account with binding of a
  clusterRole to able to interact with kubernetes APIserver.

Other end user impact
---------------------

This oran-o2 service exposes new set of APIs for SMO, but it does not affect
existing python-client of other StarlingX services

Performance Impact
------------------

The oran-o2 service should be no impact on system performance for following
reasons:

* As mentioned in problem description, oran-o2 will monitor the changes of
  sysinv resources via sysinv API, hence there is a background task polling
  the sysinv resources via sysinv API periodically. The configurable polling
  interval is in seconds level and executed in a sequential way for
  querying sysinv resources. It is expected this periodic task will not impact
  system's performance.
* There are no database queries to system.
* There is no locking to access system resource exclusively.
* The oran-o2 service is deployed as kubernetes workload hence the consumed
  resources are predicted by resource allocation at deployment phase.

Other deployer impact
---------------------

None

Developer impact
----------------

This is a new and standalone system application, it will not impact developers
working on other features.

Upgrade impact
--------------

The oran-o2 has dependencies on few sysinv API as well as the kubernetes
version, it should be upgraded accordingly if these components upgrading breaks
the dependencies.

The oran-o2 does not affect the system's upgrade since it is designed as a
standalone system application deployed over system.

The oran-o2 is implemented in cloud native way, it supports migration between hosts
by nature.

Implementation
==============

Assignee(s)
-----------

Primary assignee:
  Bin Yang

Other contributors:
  Rong Zhang
  David Liu

Repos Impacted
--------------

starlingx/app-oran-o2

Work Items
----------

* Create oran-o2 application package based on ORAN-SC release deliverable
* Create FluxCD App Package by integrating oran-o2 application to FluxCD
* Update the docs.starlingx.io for the instructions of oran-o2 application
  deployment and configuration, and API reference of oran-o2 services.

Dependencies
============


This specification depends upon the open source upstream:

O-RAN Software Community/INF project/ORAN-O2 services implementation in G
Release


Testing
=======

Functional testing will be performed on StarlingX in AIO-SX, AIO-DX,
Standard and DC configurations, which includes:

* O2 application lifecycle management in StarlingX
* O2 Use cases verification which demonstrates the Compliance of ORAN Spec Interfaces

Documentation Impact
====================

This work will primarily impact deployer and developers.

The API reference documentation will be provided for integrating with ORAN SMO.

The deployment guide document will be provided for deployer of StarlingX.

StarlingX release notes describing this feature will also be provided.

References
==========

* `WG6: O-RAN Cloudification and Orchestration Use Cases and Requirements for
  O-RAN Virtualized RAN v5.00 <https://oranalliance.atlassian.net/wiki/spaces/
  TSC/pages/2569831376/APPROVED+-+WG6+O-RAN+Cloudification+and+Orchestration+
  Use+Cases+and+Requirements+for+O-RAN+Virtualized+RAN+v5.00>`_

* `WG6: O-RAN O2ims Interface Specification v3.00 <https://oranalliance.
  atlassian.net/wiki/spaces/TSC/pages/2570617688/APPROVED+-+WG6+O-RAN+O2ims+
  Interface+Specification+v3.00>`_

* `WG6: O-RAN O2dms Interface Specification: Kubernetes Native API Profile for
  Containerized NFs v2.00 <https://oranalliance.atlassian.net/wiki/spaces/TSC/
  pages/2570715602/APRROVED+-+WG6+O-RAN+O2dms+Interface+Specification+
  Kubernetes+Native+API+Profile+for+Containerized+NFs+v2.00>`_

* `O-RAN SC INF project, ORAN-O2 services implementation in G release <https://
  gerrit.o-ran-sc.org/r/gitweb?p=pti/o2.git;a=tree;h=refs/heads/g-release>`

Acronyms
--------

- 3GPP: Third Generation Partnership Project
- 5G: Fifth-Generation Mobile Communications
- CNF: Containerized Network Function
- DMS: O-Cloud Deployment Management Services
- SMO: Service Management and Orchestration
- FOCOM: Federated O-Cloud Orchestration & Management
- IM: Information Model
- IMS: O-Cloud Infrastructure Management Services
- LCM: Life Cycle Management
- NF: Network Function
- NFO: Network Function Orchestration
- NFVI: Network Function Virtualization Infrastructure
- O-CU: O-RAN Central Unit as defined by O-RAN ALLIANCE
- O-CU-CP: O-CU Control Plane
- O-CU-UP: O-CU User Plane
- O-DU: O-RAN Distributed Unit (uses Lower-level Split)

History
=======

None
