..
 This work is licensed under a Creative Commons Attribution 3.0 Unported
 License.

 http://creativecommons.org/licenses/by/3.0/legalcode


=============================================
 Containerization: Decoupling Container Apps
=============================================

Storyboard: `#2006537`_

This spec is part of the continuing effort to manage containerized applications
built upon the StarlingX kubernetes infrastructure. Please see the background
information regarding this effort on the StarlingX `wiki`_.

This specification and supporting material will describe the effort to decouple
StarlingX application dependencies from the platform.

A StarlingX application is defined as a tarball that contains `Helm`_ charts
and an `Armada`_ manifest. Some applications also require `System Configuration
Management`_ plugins to correctly set `Helm`_ chart overrides based on the
current system deployment. These plugins are currently delivered as part of the
`System Configuration Management`_ RPM which can only be updated with a
StarlingX platform patch. This dependency ties platform patch levels to
application versions.

The goal is to provide self-contained applications that will not require
specific platform knowledge to upload, apply, and update.

.. _#2006537: https://storyboard.openstack.org/#!/story/2006537
.. _wiki: https://wiki.openstack.org/wiki/Containerizing_StarlingX_Infrastructure
.. _Helm: https://helm.sh
.. _Armada: https://airship-armada.readthedocs.io/en/latest/
.. _System Configuration Management: https://opendev.org/starlingx/config


Problem description
===================

Life-cycle management of containerized services in StarlingX uses two open
source technologies: `Helm`_, the Kubernetes package manager and `Armada`_, a
tool for managing multiple Helm charts with dependencies.

The life-cycle of an application is controlled by the `System Configuration
Management`_ sysinv API which will allow an application to be uploaded,
applied, removed, deleted, and updated.

An application is either a system application or a non-system application. A
system application is one which provides a set of `stevedore`_ plugins for each
`Helm`_ chart and the `Armada`_ manifest. Each plugin is tasked with providing
overrides for optimal support, if required, of the various StarlingX deployment
configurations.

A non-system application is an application that does not have any plugin
support and has its configuration driven entirely by the `Helm`_ chart defaults
and any `Armada`_ manifest overrides.

In StarlingX 2.0, all system applications provided their plugins as part of the
`System Configuration Management`_ sysinv package. This heavily ties specific
applications to the platform and requires a platform patch to upgrade a
StarlingX application.

This effort will strive to:

* Have the platform only provide basic life-cycle management functions for all
  applications.

* Have the platform be application agnostic. It should not contain specific
  application knowledge.

* Make the applications be self-contained in that they provide a set of
  detectable plugins upon application upload to the platform. These plugins are
  then executed by the platform under specific life-cycle conditions.

.. _stevedore: https://docs.openstack.org/stevedore/latest/


Use Cases
=========

The updates should be transparent to the end user. No new API or CLI
functionality should be required. This effort should re-factor the existing
application dependencies that are built into one or more platform packages and
incorporate the functionality into the application tarball.


Proposed change
===============

The following `design slides`_ have been prepared as a working document on this
effort. This will be updated to align with any proposed gerrit changes. This
document will:

* Identify the areas in the platform that contain specific application
  knowledge that should be decoupled from the platform.

* Provide sections for specific proposed changes to decouple the platform code
  and migrate it to the application tarball.

  * Initial efforts will focus on providing the infrastructure to support
    delivery of any existing `Helm`_ and an `Armada`_ plugins with their
    respective applications.

* Identify future areas of decoupling that can get worked on by the community.

.. _design slides: https://drive.google.com/open?id=1S60R1Sua9aOz1bw7M10QnYASKGQRdaZB



Alternatives
============

None


Data model impact
=================

None


REST API impact
===============

None


Security impact
===============

In StarlingX 2.0, the platform/application integration points are built into
various platform packages. These packages can only be updated via the StarlingX
patching mechanism. This provides a level of security as these patches are
signed and come from a validating authority.

By migrating plugins into the application tarball, applications will now upload
and execute code a part of the platform application framework. Considerations
must be taken into account to ensure that any plugin code is run in an
unprivileged manner to prevent any malicious code execution.


Other end user impact
=====================

None


Performance Impact
==================

None


Other deployer impact
=====================

When complete, StarlingX applications will be able to be upgraded independently
of the platform patch level.


Developer impact
================

Developers working in StarlingX will be able to make changes to a single
application repository once the platform ties are severed.


Upgrade impact
==============

StarlingX applications and the StarlingX platform will be able to be upgraded
independently.


Implementation
==============

Consult the `design slides`_ that will be actively updated for implementation
specifics over the course of this effort.


Assignee(s)
===========

Primary assignee:

* Robert Church (rchurch)


Repos Impacted
==============

Minimum set of repos impacted

* root
* config
* platform-armada-app
* openstack-armada-app
* monitor-armada-app


Work Items
==========

Consult the `design slides`_ that will be actively updated for implementation
specifics over the course of this effort.

Initial efforts will focus on:

* Providing application framework infrastructure support to enable plugins
  delivered with the application during application life-cycle operations.
* Build support for packaging plugins with each application
* Tox support for testing plugins
* Delivery of any existing `Helm`_ and `Armada`_ plugins with their respective
  applications. This will be done for platform-integ-apps, stx-openstack, and
  stx-monitor applications.


Dependencies
============

None


Testing
=======

The following testing will be performed in association with these proposed
changes:

* Verify that the application tarballs contain the required plugins.
* Verify the platform, openstack, and monitor applications correctly execute
  all the application life-cycle functions: upload, apply, remove, delete, and
  update.


Documentation Impact
====================

None

References
==========

References are provided throughout this document at the point when terms or
items are introduced. No additional references are needed at this time.


History
=======

.. list-table:: Revisions
   :header-rows: 1

   * - Release Name
     - Description
   * - stx-3.0
     - Introduced
