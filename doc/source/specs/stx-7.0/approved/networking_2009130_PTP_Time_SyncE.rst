..
  This work is licensed under a Creative Commons Attribution 3.0 Unported
  License. http://creativecommons.org/licenses/by/3.0/legalcode

..

======================================================
Enhance PTP Features to Support 5G Time SyncE Solution
======================================================

Storyboard:
https://storyboard.openstack.org/#!/story/2009130

For many 5G applications, clock synchronization is essential for correct
operation. The purpose of this work is to extend the PTP features of StarlingX
to support the configuration of NICs which provide Synchronous Ethernet (SyncE)
support. SyncE is a technology that allows for a reliable Pulse Per Second
(PPS) signal, such as from a GNSS source, to be distributed among NICs in order
to synchronize their clock timing. SyncE is standardized by the ITU-T in
cooperation with IEEE as G.8261, G.8262 and G.8264.

Problem description
===================

SyncE functionality requires setting values for different pins exposed by the
device driver. This means that StarlingX must provide an interface for users to
configure these pins and persist the configuration between reboots.

Use Cases
---------

The changes proposed in this document pertain to StarlingX support for the
Intel Westport Channel NIC and the related ICE driver. Other NICs may have
different configuration interfaces and will require additional development to
support. The goal of this work is to establish a basic framework that can be
extended to support additional devices in the future if required.

* Users want to configure and enable SyncE functionality on supported NICs.

Proposed change
===============

This change will introduce a new instance to the PTP instance types, called
'clock'. With this instance type, users will be able to specify host interfaces
and assign parameters to them which will be processed and stored in the sysinv
database. The existing PTP tables will be used, as the 'clock' instance type
will be handled in the same manner as ptp4l, phc2sys and ts2phc. The database
entries will then be processed into hieradata and a puppet manifest will apply
the configuration on system boot or when the user issues system
ptp-instance-apply.


Alternatives
------------

Alternatives to this approach could perhaps involve tying the SyncE
configuration into the host-if functionality of sysinv. A similar approach was
taken in the past for single-instance PTP configuration. Tying the SyncE config
to host-if or another part of sysinv risks being too restrictive to properly
support future NIC types or other more complex requirements. By including it as
a PTP instance type, there is a great deal of flexibility going forward. SyncE
configuration also shares many similarities with the configuration process for
PTP.

Data model impact
-----------------

This feature will leverage the data model changes proposed in
https://opendev.org/starlingx/specs/commit/dfaeb38ab727cbae8ad578e144fbfd42b3070089
and does not require any additional changes. SyncE configuration fits
easily within this model and only requires that the 'clock' instance type be
recognized by the API as a valid type.


REST API impact
---------------

There will be no changes to the REST API apart from those proposed in
https://opendev.org/starlingx/specs/commit/dfaeb38ab727cbae8ad578e144fbfd42b3070089

Security impact
---------------

None


Other end user impact
---------------------

None


Performance Impact
------------------

This work requires adding a small number of additional sysinv database queries
during the generation of hieradata. These queries are generally only made once
per run and the returned data is used for shared purposes among the setting up
of PTP instances as much as possible.


Other deployer impact
---------------------

None

Developer impact
----------------

None

Upgrade impact
--------------

None

Implementation
==============

Assignee(s)
-----------

Primary assignee:
  Cole Walker

Other contributors:
  Douglas Koerich
  Teresa Ho

Repos Impacted
--------------

List repositories in StarlingX that are impacted by this spec.

starlingx/config
starlingx/fault
starlingx/integ
starlingx/kernel
starlingx/monitoring
starlingx/ptp-notification-armada-app
starlingx/stx-puppet


Work Items
----------

* Update to 5.10 kernel
* Upgrade ICE driver to 1.7.16
* Upgrade linuxptp package
* Upgrade tzdata package to provide leapseconds.list
* Support 'clock' instance type
* Add handling for supported NIC parameters
* Add alarming and monitoring for SyncE
* Update ptp-notification to support GM node


Dependencies
============

Depends on:
https://review.opendev.org/c/starlingx/specs/+/835315


Testing
=======

Unit tests will be updated for APIs, as well for the hieradata generation.

Functional testing will be performed on hardware using Intel Westport
Channel NICs.

Two separate nodes with this hardware are required to properly validate SyncE
behaviour. Due to hardware limitations, initial development and testing will be
performed on AIO-SX systems, with some limited test coverage on a Standard
system. It is not expected that different system types should be relevant to
testing this feature.

Existing regression testing for the ptp-notification feature should be run
during this period in order to verify that the application still functions as
expected. It is possible that updates to the ptp-notification app may be
required as part of this work.

Documentation Impact
====================

This work will primarily affect end users. The relevant End User documentation
will need to be updated to provide configuration examples and to list the
parameters used by the clock instance type. This feature relates directly to
the Intel Westport Channel NIC and will require users to have access to Intel's
user documentation for that device in order to understand its usage.

StarlingX release notes describing this feature will be provided.

References
==========

None

History
=======

.. list-table:: Revisions
   :header-rows: 1

   * - Release Name
     - Description
   * - 1
     - Introduced
   * - 2
     - Minor syntax changes

