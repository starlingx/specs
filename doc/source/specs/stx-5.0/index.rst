StarlingX stx-5.0 Specs
=======================

Template:

.. toctree::
    :maxdepth: 1

    Specification Template (stx-5.0 release) <template>

Approved Specifications
-----------------------

.. toctree::
    :glob:
    :maxdepth: 1

    approved/*

Implemented Specifications
--------------------------

.. toctree::
    :glob:
    :maxdepth: 1

    implemented/*