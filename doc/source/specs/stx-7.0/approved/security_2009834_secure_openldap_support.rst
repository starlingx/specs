========================================
StarlingX Secure Local OpenLDAP Support
========================================

Storyboard:
https://storyboard.openstack.org/#!/story/2009834

This specification describes what configuration changes are required to support
a  Secure Local OpenLDAP. A Secure OpenLDAP server is the first building block
in transitioning to all encrypted authentication server communication,
specifically for LDAP authentication, and the future introduction of SSSD client
(System Security Services Daemon), as a replacement for the current nslcd
solution. Some valuable features that SSSD can bring that strongly support the
move to use it, are:
* Multi domain remote LDAP authentication
* Extra security by using data encryption for LDAP user authentication
* Offline authentication if a LDAP identity store is unavailable by caching
users and managing caching timeout and refresh
* High authentication and authorization performance

Problem description
===================

An SSSD solution provides a system service that establishes and manages access
to local and remote LDAP directories and other authentication servers. An SSSD
client can connect to one or more remote backend systems as opposed to a nslcd
solution that can connect to only one remote authentication backend. SSSD also
provides extra security compared to the nslcd by using data encryption for LDAP
user authentication. SSSD does not support authentication  over an unencrypted
channel. In order to work with SSSD, the local OpenLDAP server requires to be
configured as a secure OpenLDAP.

Use Cases
---------

The secure OpenLDAP configuration will be applied during the host-install step
of the installation process as well as during the host-unlock step:
* During the bootstrap step of the host-install, execute in order

  * CertManager, a Kubernetes component, creates the OpenLDAP certificate 
    from a yaml file that defines the certificate spec, only if the certificate
    was not already created.
  * apply updates to the LDAP schema configuration and copy the certificate and
    key files to the "/etc/openldap/certs" directory.

* During host-unlock step of host-install, execute in order

  * apply updates to the OpenLDAP schema configuration to add the certificate
    and key files' location.
  * update the slapd configuration to support "ldaps" protocol to allow 
    encryption of the LDAP data, and restart slapd service.
  * CertMon component starts and installs the newly created certificates as 
    well as triggering updates to OpenLDAP schema configuration and a restart of
    slapd.

* An OpenLDAP certificate gets renewed by the CertManager component before it
  reaches the expiry date. The renewal action creates a new certificate that is
  detected by the CertMon component. CertMon installs the new certificate and
  triggers OpenLDAP configuration updates to refresh the OpenLDAP certificate
  and restart slapd.
* The system command “system certificate-list” will discover and list the newly
  installed OpenLDAP certificate.
* A tool called "show-certs.sh" that gives a summary of all the certificates in
  the system will also show OpenLDAP certificate information.

Proposed change
===============

Configure a secure local OpenLDAP server.

This solution is the first step in transitioning to all encrypted LDAP
authentication server communication using SSSD system service as a replacement
for nslcd service that we currently use.
The capability to connect to local OpenLDAP using the insecure port will be kept
in this stage.
Certificate install will not be done using system cli “certificate-install” but
rather using CertManager CRDs, which will be auto-configured at bootstrap time,
and detected by CertMon that will use sysinv internal REST APIs to configure the
openldap certificate.

The following new capabilities and extensions will be added to the local
OpenLDAP configuration:

Certificate creation
--------------------

A new OpenLDAP certificate will be auto-configured at bootstrap time using the
following spec::

 apiVersion: cert-manager.io/v1alpha2
 kind: Certificate
 metadata:
   name: local-openldap-certificate
   namespace: deployment
 spec:
   secretName: local-openldap-certificate
   ipAddresses:
   - <management floating ip>
   - <management controller0_address>
   - <management controller1_address>
   commonName: local-openldap
   isCA: false
   duration: 2160h   # 90d
   renewBefore: 720h # 30d
   issuerRef:
     name: system-local-ca
     kind: ClusterIssuer

Newly added functionality will perform the following:
* The certificate spec will be loaded from a yaml file and created by
CertManager, a Kubernetes component. This is done during the bootstrap procedure
of the platform installation phase.
* Certificate’s IPaddresses will be populated at bootstrap time with the IP
addreses real values of the server where the certificate will be installed.
* Certificate will be then installed by CertMon component, together with the
other certificates, during “host-unlock” phase.

The LDAP certificate creation applies to a standalone server and to a Distributed
Cloud System Controller, but it does not apply to Distributed Cloud subclouds
because they do not have slapd running.

Certificate install
-------------------

The new LDAP certificate is installed during the host-unlock phase when the
CertMon component starts and discovers that a new certificate was created.
CertMon’s newly added LDAP certificate watcher class will call the internal
sysinv REST API “certificate_install' to trigger secure OpenLDAP schema
configuration changes, as well as slapd configuration changes followed by a
slapd service restart. The sysinv api uses a puppet runtime manifest mechanism
to trigger puppet configuration code to be executed to update OpenLDAP and slapd
configurations.

Certificate renewal
-------------------

A certificate can be renewed before it reaches the expiry date or other reasons,
like the secret is deleted. The certificate specification has a “renewBefore”
field used to trigger CertManager. A soon to be expired certificate will be
detected by CertManager and subsequently a new certificate will be created with
a new expiry date. CertMon will detect the new certificate and call internal
sysinv REST API “certificate-install” to install the certificate.
The OpenLDAP certificate renewal will follow the same pattern. The internal
sysinv REST API “certificate-install” will be used to handle OpenLDAP specific
configuration changes triggered from the CertMon’s watchers component.
The configuration changes required by a secure OpenLDAP will be performed using
the runtime manifest mechanism of the puppet component.

Certificate monitoring and alarming
-----------------------------------

This functionality is provided by the existing certificate framework.

Certificate List
----------------

The system command “system certificate-list” will discover and list the newly
installed OpenLDAP certificate.

CertMon support for OpenLDAP certificate discovery
--------------------------------------------------

CertMon will discover the new OpenLDAP certificate created by CertManager and
will use a sysinv REST API to install the certificate.
CertMon “watchers” component will be enhanced with 2 classes, a “watcher” and
a “renewal” for the OpenLDAP certificate.
The Openldap certificate renewal class will use the internal sysinv REST API
“certificate_install” to install the certificate for the first time or to update
it.

Sysinv api for OpenLDAP certificate install/renewal
---------------------------------------------------

The internal sysinv REST API “certificate_install” will interface with the puppet
component to apply the secure OpenLDAP configuration and restart "slapd"
service. The REST API will be called by the CertMon for both initial certificate
install as well as renewal.

Certificate Summary
-------------------

A tool called "show-certs.sh" gives a summary of all the certificates in the
system. The tool will be enhanced to also include the OpenLDAP certificate.

Secure OpenLDAP for System Controller
-------------------------------------

Secure OpenLDAP for System Controller follows the same pattern as for a
standalone server. Only subclouds would not have any certificate added as they
do not run slapd.

Alternatives
------------

Data model impact
-----------------

This proposal will not impact any data models.

REST API impact
---------------

Internal rest api "certificate_install" will add new option "mode=openldap" for
OpenLDAP certificate.
CLI "system certificate-install" will disallow "mode=openldap".

Security impact
---------------

There is no security impact as the new secure endpoint for openldap/slapd is not
yet being used.  In future next step, when SSSD client is introduced, security
will be enhanced with use of LDAPS (LDAP over TLS/SSL) for openldap/slapd.

Other end user impact
---------------------

Performance Impact
------------------

There is no performance impact because the LDAP configuration will not be used.
The local OpenLDAP will continue to work using the insecure communication channel.

Other deployer impact
---------------------

Developer impact
----------------

There is no developer impact.

Upgrade impact
--------------

There is no upgrade impact that be found at this time.

Implementation
==============

Assignee(s)
-----------

Primary assignee:
  TDB

Other contributors:
  TDB

Repos Impacted
--------------

The repos impacted are: config, stx-puppet, ansible-playbooks.

Work Items
----------

The following work items will be addressed:
* Generate OpenLDAP certificate and key using cert-manager
* Integrate OpenLDAP certificate into the framework provided for Default
Platform Certificates Install at bootstrap
* Update OpenLDAP configuration at bootstrap
* Update OpenLDAP configuration at host-unlock
* OpenLDAP certificate Renewal
* OpenLDAP certificate Summary

Dependencies
============

Testing
=======

Testing will involve:
* ensure insecure openldap endpoint is still working for nslcd client.
* test secure openldap endpoint using a remote sssd client.

Documentation Impact
====================

Developer documentation could have information on the new OpenLDAP certificate
in the list of platform certificates with the note that this certificate is not
currently being used.

References
==========

* https://linux.die.net/man/5/sssd.conf

History
=======

.. list-table:: Revisions
      :header-rows: 1

   * - Release Name
     - Description
   * - STX-7.0
     - Introduced

