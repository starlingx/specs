.. stx-specs documentation master file

=================================
OpenStack StarlingX Project Plans
=================================

Learn more about StarlingX project specifications for specific releases
and the process to submit a specification.

Specification content can be searched using the :ref:`search page <search>`.

Specifications
--------------

Here you can find the specs and spec template for each release:

.. toctree::
   :glob:
   :maxdepth: 1

   specs/2019.03/index
   specs/stx-3.0/index
   specs/stx-4.0/index
   specs/stx-5.0/index
   specs/stx-6.0/index
   specs/stx-7.0/index
   specs/stx-8.0/index
   specs/stx-9.0/index
   specs/stx-10.0/index

Process
-------

Documentation for stx-specs process:

.. toctree::
   :maxdepth: 1

   How to submit a spec <specs/instructions>

For more details, look at spec template for the specific release, and see the
wiki page on Blueprints: https://wiki.openstack.org/wiki/Blueprints
