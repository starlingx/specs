..
  This work is licensed under a Creative Commons Attribution 3.0 Unported
  License. http://creativecommons.org/licenses/by/3.0/legalcode

========================================
StarlingX Auditd Support
========================================

Storyboard:
https://storyboard.openstack.org/#!/story/2008849

This specification describes a Linux Auditing System containerized solution
for StarlingX.

The Linux Auditing System helps system administrators track security
violation events based on pre-configured audit rules. The events are recorded
in a log file and the information in the log entries helps to detect misuse
or unauthorized activities. Some example of auditable events are:
* file or directory access (accessed, modified, executed, or attributes have
been changed)
* file or directory removal
* system calls (e.g.: useradd, time-related system calls)
* commands run by a user (For example, a rule can be defined for every
executable in the /bin directory and tracked per user)
* security events, such as failed login attempts
* network access (The iptables and ebtables utilities can be configured to
trigger Audit events)

Audit system does not provide additional security to a system, rather, it
helps detect any violations of system security policies that subsequently
lead to security measures to prevent them.


Problem description
===================

The Linux Auditing System is disabled in Starlingx and there is no mechanism
for an end user to enable it.

There are concerns over the system performance degradation when enabling the
Linux Auditing System.

However, some end users want it enabled for security reasons, and are willing
to accept the system performance degradation.


Use Cases
---------

Audit system can be optionally deployed. In the case where the audit system is
to be deployed, as a system administrator one can perform the following
actions:

* Upload and apply audit armada application.
* Verify auditd daemon is started.
* Configure more audit rules for auditable events adding to the default rules.
* Create auditable events on the host.
* View auditable events log entries on the host in /var/log/audit/audit.log.

Proposed change
===============

Audit System support using a containerized solution.

This approach allows for optional deployment of the Audit System and is
consistent with the general long term direction to containerize components
of the StarlingX flock.

Auditd will be packaged as an armada application that will be
included as RPM in an ISO image. The auditd armada application will not be
uploaded and applied by default. The end user will decide if the auditd
armada application will be used, given the performance degradation that
comes with running auditd.

The auditd armada application will have a single helm chart that deploys a
DaemonSet configuration that creates a super-privileged auditd pod/container
on each host in the StarlingX cluster. Auditd daemon running in a
super-privileged pod/container allows reporting on
auditable events created on the host. Adding a hostPath volume configuration
for the pod/container will mount the "/var/log/audit" path from the host
node's filesystem into the pod. This way the audit.log generated by the auditd
container can be persisted and viewed on the host.

Creating a DaemonSet configuration for auditd container ensures that auditd
will run on all kubernetes nodes, such as controllers, all-in-one
controllers and worker nodes.

Auditd's main configuration is managed in file “/etc/audit/auditd.conf”.
This file consists of configuration parameters that include among others,
where to log events, log format, number of audit log files kept, how to deal
with full disks, and log rotation. For example, to configure the maximum log
file size in MB, parameter "max_log_file" gets set, and for the action to take
once the size is reached, parameter "max_log_file_action" is set to "ROTATE".
The content of auditd.conf will be passed into the auditd container/pod
through a ConfigMap populated by a helm chart variable. When changes are made
to the audit configuration by updating the corresponding helm chart variable,
the re-apply of the auditd armada application will restart the auditd
DaemonSet.

The other audit configuration is for "audit rules" that control what events
will be logged.  File "/etc/audit/rules.d/audit.rules" is used to add new
rules. Rules are persisted in file "/etc/audit/audit.rules".  Again the
content of audit.rules will be passed into the auditd container/pod through a
ConfigMap populated by a helm chart variable.  The auditd armada application
will provide a set of default audit rules. The end user will be able to update
the audit rules through the helm chart overrides.

There will be a single version of the auditd.conf and audit.rules
configuration that will be used in all auditd DaemonSets across all hosts in
StarlingX cloud.

This change will also require enabling auditd in the host kernel by setting
to 1 the kernel parameter "audit". The change gets done in file
"/etc/default/grub". Then the change needs to be applied to GRUB 2
configuration file "grub.cfg".

Alternatives
------------

A host-based solution would have been an alternative solution.  However this
would have required additional development of 'system service-parameters ...'
REST APIs / CLIs in order to manage the enabling/disabling of auditd and
configuring of auditd.  The containerized solution is better because the
multi-line helm chart variable for populating the auditd.conf and audit.rules
ConfigMaps will basically have the identical format as the corresponding
auditd files.

Also a containerized solution is consistent with the general long term
direction to containerize components of the StarlingX flock.


Data model impact
-----------------

This proposal will not impact any data models.

REST API impact
---------------

The change will not impact the various REST APIs that are part of the flock.

Security impact
---------------

The Linux Auditing System helps system administrators track security
violation events based on pre-configured audit rules. The events are recorded
in a log file and the information in the log entries helps to detect misuse
or unauthorized activities.

Audit system does not provide additional security to a system, rather, it
helps detect any violations of system security policies that subsequently
lead to security measures to prevent them.

Other end user impact
---------------------

This change will not affect any of the python clients.

Performance Impact
------------------

Auditd is known to have a performance impact when running.
There will be some measurement work done and characterization of performance
impact of running with auditd enabled.

Other deployer impact
---------------------

Developer impact
----------------

None.

Upgrade impact
--------------

None.

Implementation
==============

Assignee(s)
-----------

Primary assignee:
  Carmen Rata

Other contributors:
  None.

Repos Impacted
--------------

A new repo will be created for the auditd armada application.

Work Items
----------

Dependencies
============

Testing
=======

Testing will involve:
* installing the armada application
* verifying the creation of the auditd container
* create an auditable event on the host and verify that is logged in the
host's audit log file "/var/log/audit/audit.log".

Documentation Impact
====================

Developer documentation will need to have information on audit rules update
through helm chart overrides.
Some other information that is useful to be documented will be:
* enable auditd on the host
* upload and apply the auditd armada application
* update auditd.conf

References
==========

* https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/security_guide/chap-system_auditing

History
=======

.. list-table:: Revisions
   :header-rows: 1

   * - Release Name
     - Description
   * - STX-6.0
     - Introduced
