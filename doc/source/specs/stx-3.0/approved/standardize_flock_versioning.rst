..
  This work is licensed under a Creative Commons Attribution 3.0 Unported
  License. http://creativecommons.org/licenses/by/3.0/legalcode

====================================
Standardize Flock Package Versioning
====================================

Problem description
===================

Currently all original StarlingX services and packages have a common base
version of 1.0 or 1.0.0 as defined in the rpm specfiles, before
TIS_PATCH_VER is applied. The build system creates tarballs and whl files
with the base version (without TIS_PATCH_VER) and the version is also
attached the generated RPM file (with TIS_PATCH_VER). While this version
is intended to be an API version, it is also being applied to packaging.

While the RPM package versions are unique based on the %{tis_patch_ver}
being part of the "Release" component of the RPM package name, this does
not fully reflect the actual version of the packages in the repo. The
proposed approach would initially version all packages in a repo with the
same PBR generated %{version} with the unique %{release} containing
tis_patch_ver value (or something similar).

A common versioning approach is highly beneficial across all of these
components.  This supports the packaging and re-use of these components in
multiple Linux distributions and in containers.

Every packaging toolchain has its own build recipe requirements to create
packages. They usually require the ability to generate a proper version from
the source code or code repository. StarlingX leverages much of the tooling
used by OpenStack projects. Having a versioning scheme that deviates
significantly from those used by OpenStack creates additional work and
cognitive load for developers and consumers of these components.

Use Cases
=========

* Publishing packages to RPM repositories requires a predictable versioning
  scheme. OS packaging expects a scheme approximating semantic versioning [0].

* Independent component versions allow interim code releases between the
  integrated StarlingX releases in the future. This is most useful in library
  and client components, such that they can be consumed by independently of the
  integrated release.


Proposed change
===============

StarlingX will adopt the OpenStack Semantic Versioning [0] model based on the
usual MAJOR.MINOR.PATCH format where:

* MAJOR version increment indicates incompatible API changes or a new
  integrated release
* MINOR version increment indicates new backward-compatible functionality or
  dependency changes
* PATCH version increment indicates backward-compatible bug fixes

OpenStack's Python Build Reasonableness (PBR) [1] will be used to generate the
version by parsing semantically-versioned Git tags and walking the Git history
back to the last release tag. Each commit is denoted in the generated version
by an additional component based on the git commit-ish or an incrementing value
as described in the PBR documentation.

The Release string which contains the %{tis_patch_ver} will remain unchanged
and still appended to the final version string of the RPM.

For example, with a git tag of "2.0.0" in a repo and a TIS_PATCH_VER=7 for a
package in that repo, PBR will generate the version based on this tag and set
%{version} to 2.0.0, and the %{release} would be 7.tis. The final package
name would be <package>-2.0.0-7.tis.<arch>.rpm. Future version numbers would
be based on the number of commits from the 2.0.0 tag, so 5 commits later the
version string will be 2.0.1.dev5-7.tis (assuming TIS_PATCH_VER is unchanged).

Git repositories:

* Add Semantically-versioned Git tags to all flock package git repos.

* Packages in a single repo have the same version.

* Versioning is managed per repo, for example one repo could be at 2.0.1
  and another at 2.0.4 if there has been more change to the second repo.

* This process, independent of policy, supports both keeping the major
  versions synced with the integrated release or allowing them to diverge.

Build scripts and RPM spec files need to be improved:

* Build scripts need to be improved to use PBR to generate a proper version
  from the Semantically-versioned Git tags.

* Build scripts will use rpmbuild tool to define "pkg_version" for SRPM build.
  The spec file should be improved to accept it from build scripts.

* Build scripts will continue to use TIS_PATCH_VER as defined in
  build_srpm.data.

Alternatives
------------

Manage the package version manually. Most version will remain at 1.0 along
with the TIS_PATCH_VERSION info.

Data model impact
-----------------

None

REST API impact
---------------

REST APIs is versioned separately as is done in many OpenStack projects to
allow them to evolve and be detectable by API consumers.

Security impact
---------------

None

Other end user impact
---------------------

None

Performance Impact
------------------

None

Other deployer impact
---------------------

None

Developer impact
----------------

To add new packages, it needs to update the spec file to accept "pkg_version"
definition from build scripts.

Releases (any version change) is handled by the release tooling. It does not
need developer practice.

Upgrade impact
--------------

None

This change only applies to flock packages (e.g. stx-fault).

TIS_PATCH_VERSION is removed from the package version. It should be handled
similar to existing upstream packages, e.g. from CentOS, that are unmodified
and have no TIS_PATCH_VERSION.


Implementation
==============

Assignee(s)
-----------

Primary assignee:
  Bin Yang (byangintel)

Other contributors:
  Saul Wold (sgw-starlingx)

Repos Impacted
--------------

* All repos will be tagged
  except docs, elections, governance, and specs
* Repos that generate artifact such as:
  * starlingx/ansible-playbooks
  * starlingx/clients
  * starlingx/config
  * starlingx/fault
  * starlingx/gui
  * starlingx/ha
  * starlingx/metal
  * starlingx/nfv
  * starlingx/update

Work Items
----------

* Add setup.py and setup.cfg in non-pythonic repo for PBR to generate version.

* Improve the build scripts to generate package version with PBR.

* Improve the RPM spec file to accept the version from build scripts.

* Add git tag for version generation.

Dependencies
============

None

Testing
=======

This change should be transparent for the common developer, so no changes
should be noticed after this implementation. The proposed testing is:

* To have a full clean build: The build procedure shall be transparent for the
  developer.

* Check the impacted packages' version manually.

* Validate installation is completed correctly and initial boot deployment
  works

Documentation Impact
====================

Add a documentation to explain the version generation.

References
==========

[0] https://docs.openstack.org/pbr/latest/user/semver.html

[1] https://docs.openstack.org/pbr/latest/

History
=======

