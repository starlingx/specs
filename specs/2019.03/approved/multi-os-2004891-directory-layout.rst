..  This work is licensed under a Creative Commons Attribution 3.0 Unported
    License.
    http://creativecommons.org/licenses/by/3.0/legalcode

==============================================
StarlingX: Define directory layout for MultiOS
==============================================

Storyboard: https://storyboard.openstack.org/#!/story/2004891

This specification will extend the current Flock services directory structure
to support more operating systems apart from the current CentOS OS supported.


Problem description
===================

The current directory structure is based around the build working with CentOS
OS and the current build tooling. The flock services git repositories provide a
directory call centos with the spec files to build RPMs distribution packages.
In order to support building the flock services packages for new, different
OSes is necessary to provide additional directories where the community can
have a place to contribute with build scripts such as control and rules files
(for Debian based operating systems) or spec files based on another RPM base OS
like openSUSE

Use Cases
=========

a) Users that want to build the flock services on other OSes beyond the
current CentOS fully supported by Starling X community


Proposed change
===============

Define an extended directory layout that will support other Operating Systems
beyond the existing CentOS layout. The existing layout will remain in place and
will not disrupt the current workflow in the CentOS build system.

The following is the proposed layout, where <package-name> could be any one of
packages that build flock services.

https://opendev.org/starlingx/fault/src/branch/master/fm-common

::

    <package-name>
      ├── <existing source>
      │   ├── source code of the project
      ├── centos //This is the current centOS directory, it will not be modified.
      └── debian
      │   ├── fm-common-dev.install
      │   ├── fm-common-doc.install
      │   ├── fm-common-wheels.install
      │   ├── fm-common.install
      │   ├── rules
      │   ├── control
      │
      └── opensuse
      │   ├──fm-common.spec

The Debian directory will contain the following files:

* Debian/control:

The Debian/control file contains the most vital (and version-independent)
information about the source package and about the binary packages it creates.

* Debian/rules:

The Debian/rules script is the executable script to build the Debian package.

The openSuSE directory will contain the spec file for open SuSE distribution.
The build dependencies required are not the same that the spec file from centOS

The proposed change is only to provide a place where the StarlingX community
could leave the build recipes for other distributions. The only distribution
fully supported will be CentOS.

Alternatives
============

Create a new set of directories to represent each new operating system.

Data model impact
=================

None


REST API impact
===============

None

Security impact
===============

None

Other end user impact
=====================

None


Performance Impact
==================

None

Other Deployer impact
=====================

None

Developer impact
=================

The community might send patches to

https://opendev.org/starlingx/config
https://opendev.org/starlingx/distcloud
https://opendev.org/starlingx/distcloud-client
https://opendev.org/starlingx/fault
https://opendev.org/starlingx/gui
https://opendev.org/starlingx/ha
https://opendev.org/starlingx/integ
https://opendev.org/starlingx/nfv
https://opendev.org/starlingx/update
https://opendev.org/starlingx/metal

Upgrade impact
===============

None

Implementation
==============

If a community member wants to provide the build recipes to build a flock the
component in other OS it will be necessary to send the git-review of the patch
adding these files into the proper directory with the name of the os.

These files will be created  following upstream distro packaging guidelines
(for example the openSUSE spec file should follow the openSUSE Guideline and
validate with spec-cleaner tools[1])

Assignee(s)
===========

Primary assignee:
   - Victor Rodriguez

Other contributors:
   - Marcela Rosales
   - Erich Cordoba Malibran

Repos Impacted
==============

Work Items
===========

Create directory tree and files as new Operating Systems are added by
community.

Dependencies
============


Testing
=======

Ensure that the current build continues to work as the directory layout is
extended.

For each type of packaging meta-data (specfile, rules, control, ...) for a
specific distribution, there should be a minimum set of gating jobs to validate
that meta-data. This may vary from simple linter type of job (spec-cleaner
tools[1] for OpenSUSE), to full blown build of the package and validate the
built package somehow (rpmlint). This does not necessarily mean validating the
actual packaged binaries, just the packaging itself.

Documentation Impact
====================

New documentation will be generated to define the contents of the extended
directory layout.

Every time a new OS build recipe is provided by the community, the proper
documentation of how to build the package for specific distro should be
provided.

References
==========

[1]https://github.com/openSUSE/spec-cleaner

History
=======

.. list-table:: Revisions
      :header-rows: 1

   * - Release Name
     - Description
   * - STX-2.0
     - Introduced

